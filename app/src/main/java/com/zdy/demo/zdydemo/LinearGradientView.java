package com.zdy.demo.zdydemo;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by zhangdongyang on 2016/6/13.
 */
public class LinearGradientView extends TextView {

    public LinearGradientView(Context context) {
        super(context);
    }

    boolean aBoolean;

    public LinearGradientView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (attrs != null) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ZdyTest, 0, 0);
            aBoolean = a.getBoolean(R.styleable.ZdyTest_is_select, false);
        }
    }


    @Override
    protected void onDraw(Canvas canvas) {
        mGradientMatrix.setTranslate(mTranslate, 0);
        mLinearGradient.setLocalMatrix(mGradientMatrix);
        super.onDraw(canvas);
    }

    //白色左推红色
    public void setTranslate3(float ds) {
        mTranslate = -mViewWidth * ds + mViewWidth * 0.5f;
        if (mTranslate >= mViewHeight * 1.5f)
            mTranslate = mViewHeight * 1.5f;
        postInvalidate();
    }

    //红色左推白色
    public void setTranslate2(float ds) {
        mTranslate = -mViewWidth * ds + mViewWidth * 1.5f;
        if (mTranslate >= mViewHeight * 1.5f)
            mTranslate = mViewHeight * 1.5f;
        postInvalidate();
    }

    //白色右推红色
    public void setTranslate(float ds) {
        mTranslate = mViewWidth * ds + mViewWidth / 2;
        if (mTranslate >= mViewHeight * 1.5f)
            mTranslate = mViewHeight * 1.5f;
        postInvalidate();
    }

    //红色右推白色
    public void setTranslate1(float ds) {
        mTranslate = mViewWidth * ds - mViewHeight * 0.5f;
        if (mTranslate <= -mViewHeight * 0.5f)
            mTranslate = -mViewHeight * 0.5f;
        postInvalidate();
    }

    public void setSelect(boolean isSelect) {
        if (!isSelect) {
            mTranslate = mViewWidth / 2;
        } else {
            mTranslate = -mViewWidth / 2;
        }
        postInvalidate();
    }

    private LinearGradient mLinearGradient;
    private Matrix mGradientMatrix;
    private Paint mPaint;
    private int mViewWidth = 0;
    private float mTranslate = 0;
    private int mViewHeight = 0;

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if (mViewWidth == 0 || mViewHeight == 0) {
            mViewWidth = getMeasuredWidth();
            mViewHeight = getMeasuredWidth();
            if (mViewWidth > 0) {
                mPaint = getPaint();
                mLinearGradient = new LinearGradient(0, mViewHeight / 2, mViewWidth, mViewHeight / 2,
                        new int[]{Color.RED, Color.WHITE},
                        new float[]{0.5f, 0.5f}, Shader.TileMode.MIRROR);
                mPaint.setShader(mLinearGradient);
                mGradientMatrix = new Matrix();
                if (aBoolean) {
                    mTranslate = -mViewWidth / 2;
                } else {
                    mTranslate = mViewWidth / 2;
                }
                mGradientMatrix.setTranslate(mTranslate, 0);
                mLinearGradient.setLocalMatrix(mGradientMatrix);
            }
        }
    }
}
