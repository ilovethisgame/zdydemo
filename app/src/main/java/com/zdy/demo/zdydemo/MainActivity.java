package com.zdy.demo.zdydemo;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private ViewPager vp;
    LinearGradientView test1, test2, test3;
    List<View> views;
    private int lastValue;
    private int curPos = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        vp = (ViewPager) findViewById(R.id.vp);
        test1 = (LinearGradientView) findViewById(R.id.test1);
        test2 = (LinearGradientView) findViewById(R.id.test2);
        test3 = (LinearGradientView) findViewById(R.id.test3);
        test1.setOnClickListener(this);
        test2.setOnClickListener(this);
        test3.setOnClickListener(this);
        views = new ArrayList<>();
        TextView textView = new TextView(this);
        textView.setTextColor(Color.BLACK);
        textView.setGravity(Gravity.CENTER);
        textView.setText("左右滑动切换1");
        views.add(textView);
        TextView textView1 = new TextView(this);
        textView1.setTextColor(Color.BLACK);
        textView1.setGravity(Gravity.CENTER);
        textView1.setText("左右滑动切换2");
        views.add(textView1);
        TextView textView2 = new TextView(this);
        textView2.setTextColor(Color.BLACK);
        textView2.setGravity(Gravity.CENTER);
        textView2.setText("左右滑动切换3");
        views.add(textView2);
        vp.setAdapter(new MyAdapter());
        vp.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                System.err.println("position==" + position);
                System.err.println("positionOffset==" + positionOffset);
                System.err.println("positionOffsetPixels==" + positionOffsetPixels);
                if (lastValue > positionOffsetPixels && positionOffsetPixels != 0) {
                    // 递减，向右侧滑动
                    if (position == 0) {
                        test1.setTranslate3(1 - positionOffset);
                        test2.setTranslate2(1 - positionOffset);
                    } else {
                        test2.setTranslate3(1 - positionOffset);
                        test3.setTranslate2(1 - positionOffset);
                    }
                } else if (lastValue < positionOffsetPixels && positionOffsetPixels != 0) {
                    // 递增，向左侧滑动
                    if (position == 1) {
                        test2.setTranslate1(positionOffset);
                        test3.setTranslate(positionOffset);
                    } else {
                        test1.setTranslate1(positionOffset);
                        test2.setTranslate(positionOffset);
                    }
                }

                lastValue = positionOffsetPixels;
            }

            @Override
            public void onPageSelected(int position) {
                curPos = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    private void setTitleTextColor(int pos) {
        switch (pos) {
            case 0:
                test1.setSelect(true);
                test2.setSelect(false);
                test3.setSelect(false);
                vp.setCurrentItem(0, false);
                break;
            case 1:
                test1.setSelect(false);
                test2.setSelect(true);
                test3.setSelect(false);
                vp.setCurrentItem(1, false);
                break;
            case 2:
                test1.setSelect(false);
                test2.setSelect(false);
                test3.setSelect(true);
                vp.setCurrentItem(2, false);
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.test1:
                setTitleTextColor(0);
                break;
            case R.id.test2:
                setTitleTextColor(1);
                break;
            case R.id.test3:
                setTitleTextColor(2);
                break;
        }
    }


    class MyAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return views.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(ViewGroup view, int position, Object object) {
            view.removeView(views.get(position));
        }

        @Override
        public Object instantiateItem(ViewGroup view, int position) {
            view.addView(views.get(position));
            return views.get(position);
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if (hasFocus) {
            setTitleTextColor(curPos);
        }
        super.onWindowFocusChanged(hasFocus);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null)
            curPos = savedInstanceState.getInt("currentPos");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (outState != null)
            outState.putInt("currentPos", curPos);
    }
}
